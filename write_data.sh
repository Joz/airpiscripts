#!/bin/bash

DEVICE=DATA
MNT_PI=/mnt/usb

set -e

mkdir -p $MNT_PI
umount $MNT_PI || echo "(erwartungsgemäß nicht gemountet)"
mount -L $DEVICE $MNT_PI
mkdir -p /tmp/csv
cd /tmp/csv/
#foto ordner erstellen
mkdir -p "$MNT_PI/data/DCIM"
#csv ordner erstellen
mkdir -p "$MNT_PI/data/csv"

#bilder auf usb
mv *.jpg "$MNT_PI/data/DCIM"


for i in $(ls) ; do
    echo "Konkateniere $i…"
    mv "$i" "$i.bak"
    #touch "$MNT_PI/data/csv/$i"
    cat "$i.bak" >> "$MNT_PI/data/csv/$i"
    rm "$i.bak"
done

umount $MNT_PI
sync
