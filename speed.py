#!/usr/bin/python

#import section
import RPi.GPIO as GPIO
from time import *

#use P1 header pin numbering convention
GPIO.setmode(GPIO.BCM)

counter = 0
dt = 0

#Pulldown Widerstand am Pin aktivieren und als INPUT deklarieren
GPIO.setup(14,GPIO.IN,pull_up_down = GPIO.PUD_DOWN)

def windSpeed(channel):

    #globale Variable
    global counter
    counter = counter + 1




def calculate():


    freq = counter

    value = (freq * 0.07881 + 0.32) / 10

    print("frequency: " + str(value))



GPIO.add_event_detect(14,GPIO.RISING,callback = windSpeed)


while True:

        sleep(10)
        calculate()
        counter = 0
