#!/bin/bash



PIDEVICE=DATA
PIDIR=/mnt/usb


set -e

mkdir -p $PIDIR

if mount -L $PIDEVICE $PIDIR || echo error=$(mount -L $PIDEVICE $PIDIR) ;
then

    mkdir -p /mnt/usb/data/temp

    /home/pi/airpiscripts/ds1820.py

    umount $PIDIR
    sync
fi
