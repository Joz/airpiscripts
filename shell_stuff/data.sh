#!/bin/bash



PIDEVICE=DATA
PIDIR=/mnt/usb

set -e

mkdir -p $PIDIR

if mount -L $PIDEVICE $PIDIR || echo error=$(mount -L $PIDEVICE $PIDIR) ;
then
    mkdir -p /mnt/usb/data/dcim
    DATE=$(date +"%Y-%m_%d_%H%M")
    raspistill -vf -hf -o /mnt/usb/data/dcim/$DATE.jpg
    umount $PIDIR
    sync
fi
