#!/bin/bash



PIDEVICE=DATA
PIDIR=/mnt/usb

set -e

mkdir -p $PIDIR

if mount -L $PIDEVICE $PIDIR || echo error=$(mount -L $PIDEVICE $PIDIR) ;
then
    mkdir -p /mnt/usb/data/luftdruck

    /home/pi/airpiscripts/Adafruit-Raspberry-Pi-Python-Code/Adafruit_BMP085/Adafruit_BMP085_example.py

    umount $PIDIR
    sync
fi
