#!/bin/bash

DATE=$(date +"%Y-%m_%d_%H%M")
raspistill -vf -hf -o /tmp/$DATE.jpg

mv /tmp/$DATE.jpg /tmp/csv
